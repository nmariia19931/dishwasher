package ru.tsystems.autotest;

import ru.tsystems.autotest.customExceptions.CapacityExceededException;

/**
 * Class to simulate the working process of the dishwasher
 */
public class Main {

    public static void main(String[] args) {
//        //the code line to check IllegalArgumentException() call for maxCapacity validation in the constructor
//        Dishwasher dishwasher1 = new Dishwasher(-3);
        Dishwasher dishwasher = new Dishwasher(3);
        System.out.println("Max Capacity: " + dishwasher.getMaxCapacity());
        System.out.println("Current Capacity: " + dishwasher.getCurrentCapacity());
        System.out.println("Status: " + dishwasher.getStatus());
//        //the code line to check IncorrectStartException()
//        dishwasher.startDishwasher();
//        //the code line to check IncorrectStopException()
//        dishwasher.stopDishwasher();
//        //the code line to check IncorrectExtractingException()
//        dishwasher.extractAllDishes();
        try {
            dishwasher.uploadItem();
            dishwasher.uploadItem();
            dishwasher.uploadItem();
            dishwasher.uploadItem();
            dishwasher.uploadItem();
        } catch (CapacityExceededException e) {
            System.out.println("Unable to add one more item cause: " + e.getMessage());
        }
        System.out.println("Current Capacity: " + dishwasher.getCurrentCapacity());
//        //the code line to check CapacityExceededException()
//        dishwasher.uploadItem();
        dishwasher.startDishwasher();
        System.out.println("Status: " + dishwasher.getStatus());
//        //the code line to check OpenRunningDishwasherException()
//       dishwasher.uploadItem();
//        //the code line to check IncorrectExtractingException()
//        dishwasher.extractAllDishes();
        dishwasher.stopDishwasher();
        System.out.println("Status: " + dishwasher.getStatus());
//        //the code line to check CleanDishersInDishwasherException()
//       dishwasher.uploadItem();
//        //the code line to check IncorrectStartException()
//        dishwasher.startDishwasher();
        dishwasher.extractAllDishes();
        System.out.println("Status: " + dishwasher.getStatus());
//        //the code line to check IncorrectStartException()
//       dishwasher.startDishwasher();
//        //the code line to check IncorrectStopException()
//        dishwasher.stopDishwasher();
    }
}
