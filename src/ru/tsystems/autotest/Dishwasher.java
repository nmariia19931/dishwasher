package ru.tsystems.autotest;

import ru.tsystems.autotest.customExceptions.*;

/**
 * Class contains all possible functionality of the dishwasher.
 */
public class Dishwasher {
    /**
     * maxCapacity - max count of items in dishwasher.
     */
    private int maxCapacity;
    /**
     * currentCapacity - count of items in dishwasher at the moment.
     */
    private int currentCapacity;
    /**
     * status - the state of dishwasher.
     */
    private Status status;

    /**
     *
     * @param maxCapacity defines max count of items in dishwasher.
     * @exception  IllegalArgumentException  if the maxCapacity is less or equal 0.
     */
    public Dishwasher(int maxCapacity) {
        if (maxCapacity > 0) {
            this.maxCapacity = maxCapacity;
            this.currentCapacity = 0;
            this.status = Status.UPLOADING;

        } else {
            throw new IllegalArgumentException("Max capacity should be greater than 0!");
        }
    }

    /**
     * Method to get the defined max count of items in dishwasher.
     * @return max count of items in dishwasher.
     */
    public int getMaxCapacity() {
        return maxCapacity;
    }

    /**
     * Method to get the current count of items in dishwasher.
     * @return the current count of items in dishwasher.
     */
    public int getCurrentCapacity() {
        return currentCapacity;
    }

    /**
     * Method to get the current state of the dishwasher.
     * @return the current state of the dishwasher.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Method simulates uploading one item in the dishwasher.
     * @throws CapacityExceededException if the current count of items in th dishwasher is equals to the max capacity.
     * @exception CleanDishersInDishwasherException if the clean dishes were not extracted after the dishwasher were
     * switched to STOPPED status.
     * @exception OpenRunningDishwasherException if the uploading item takes place when the dishwasher is in
     * RUNNING status.
     */
    public void uploadItem() throws CapacityExceededException{
        System.out.println("///////////////////////////////////////");
        System.out.println("The button UPLOAD was pushed...");
        if (status != Status.RUNNING) {
            if (status != Status.STOPPED) {
                if (currentCapacity < maxCapacity) {
                    currentCapacity += 1;
                    System.out.println("One item was uploaded.");
                } else {
                    throw new CapacityExceededException("The dishwasher's capacity was exceeded!");
                }
            } else {
                throw new CleanDishersInDishwasherException("Dishwasher contains clean dishers! Extract it first!");
            }
        } else {
            throw new OpenRunningDishwasherException("Don't open running dishwasher!");
        }
    }

    /**
     * Method simulates start of the dishwasher.
     * @exception IncorrectStartException if the clean dishes were not extracted after the dishwasher were
     * switched to STOPPED status or the dishwasher is empty or the dishwasher is already running
     */
    public void startDishwasher() {
        System.out.println("///////////////////////////////////////");
        System.out.println("The button START was pushed...");
        if (status != Status.RUNNING) {
            if (currentCapacity != 0) {
                if (status != Status.STOPPED) {
                    status = Status.RUNNING;
                    System.out.println("The dishwasher was ran");
                } else {
                    throw new IncorrectStartException("The dishwasher contains clean dishes! Please, extract clean dishers and upload new one!");
                }
            } else {
                throw new IncorrectStartException("The dishwasher is empty! Please, upload the dishers!");
            }
        } else {
            throw new IncorrectStartException("The dishwasher is already running!");
        }
    }

    /**
     * Method simulates stop of the dishwasher.
     * @exception IncorrectStopException if the dishwasher has been already stopped or if the dishwasher
     * isn't running yet
     */
    public void stopDishwasher() {
        System.out.println("///////////////////////////////////////");
        System.out.println("The button STOP was pushed...");
        if (status != Status.UPLOADING) {
            if (status != Status.STOPPED) {
                status = Status.STOPPED;
                System.out.println("The dishwasher was stopped");
            } else {
                throw new IncorrectStopException("The dishwasher has been already stopped! Please, extract the dishes!");
            }
        } else {
            throw new IncorrectStopException("The dishwasher isn't running yet!");
        }
    }

    /**
     * Method simulates stop of the dishwasher.
     * @exception IncorrectExtractingException if the dishwasher is empty or if the dishwasher is already running
     */
    public void extractAllDishes() {
        System.out.println("///////////////////////////////////////");
        System.out.println("The button EXTRACT was pushed...");
        if (status != Status.RUNNING) {
            if (currentCapacity != 0) {
                currentCapacity = 0;
                status = Status.UPLOADING;
                System.out.println("All dishes were extracted.");
            } else {
                throw new IncorrectExtractingException("The dishwasher is empty! Nothing to extract!");
            }
        } else {
            throw new IncorrectExtractingException("The dishwasher is already running! Please, wait until it stops!");
        }
    }
}
