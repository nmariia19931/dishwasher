package ru.tsystems.autotest.customExceptions;

public class IncorrectExtractingException extends RuntimeException {
    public IncorrectExtractingException(String errorMessage) {
        super(errorMessage);
    }
}
