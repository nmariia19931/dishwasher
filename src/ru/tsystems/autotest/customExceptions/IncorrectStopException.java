package ru.tsystems.autotest.customExceptions;

public class IncorrectStopException extends RuntimeException {
    public IncorrectStopException(String errorMessage) {
        super(errorMessage);
    }
}
