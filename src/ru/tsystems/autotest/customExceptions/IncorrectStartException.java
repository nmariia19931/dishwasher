package ru.tsystems.autotest.customExceptions;

public class IncorrectStartException extends RuntimeException {
    public IncorrectStartException(String errorMessage) {
        super(errorMessage);
    }
}
