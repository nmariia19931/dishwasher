package ru.tsystems.autotest.customExceptions;

public class CapacityExceededException extends Exception {
    public CapacityExceededException(String errorMessage){
        super(errorMessage);
    }
}
