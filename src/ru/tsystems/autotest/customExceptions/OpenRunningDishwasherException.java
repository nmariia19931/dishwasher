package ru.tsystems.autotest.customExceptions;

public class OpenRunningDishwasherException extends RuntimeException {
    public OpenRunningDishwasherException(String errorMessage) {
        super(errorMessage);
    }
}
