package ru.tsystems.autotest.customExceptions;

public class CleanDishersInDishwasherException extends RuntimeException {
    public CleanDishersInDishwasherException(String errorMesssage) {
        super(errorMesssage);
    }
}
