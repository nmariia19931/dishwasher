package ru.tsystems.autotest;

/**
 * Enum contains the dishwasher's states
 * UPLOADING -> RUNNING -> STOPPED(contains clean dishes)
 */
public enum Status {
    UPLOADING,
    RUNNING,
    STOPPED;
}
